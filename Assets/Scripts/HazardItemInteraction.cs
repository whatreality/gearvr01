﻿using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.UI;



// This script is use to handle interactions with hazard items.
// It relies on VRStandardAssets.Utils for the event handling.

public class HazardItemInteraction : MonoBehaviour
{
    [SerializeField] private VRInteractiveItem m_InteractiveItem; //add script to item and attach to this

    //materials for actions
    [SerializeField] private Material m_NormalMaterial;
    [SerializeField] private Material m_OverMaterial;
    [SerializeField] private Material m_ClickedMaterial;
    [SerializeField] private Material m_DoubleClickedMaterial;
    [SerializeField] private Renderer m_Renderer; // item that receives the action materials
    

    [SerializeField] private bool m_IsFound;    //tracks if the item has already been tagged as found
    [SerializeField] private GameObject m_ShowHideObject; // which particvle system or other object to show or hide when hovering
    public AudioSource m_audio; //sound when hovering

    public int scoreValue;
    public GameStatus gameStatus;

    //Text used for item details
    public string HazardTitle;
    public string HazardDescription;
    public string Risks;
    public string Controls;
    public GameObject m_canvas; //The Canvas holding the item details UI and objects



    private void Start()
    {
        m_ShowHideObject.SetActive(false);
       // m_canvas.SetActive(false);  Using the initial UI visible as a set of instructions
                
    }

    private void Awake()
    {
        m_Renderer.material = m_NormalMaterial;
    }


    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_InteractiveItem.OnClick += HandleClick;
        m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_InteractiveItem.OnClick -= HandleClick;
        m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
    }


    //Handle the Over event - Change material, turn particle system on and play any sfx
    private void HandleOver()
    {
        Debug.Log("Show over state");
        if (m_ShowHideObject != null) m_ShowHideObject.SetActive(true);
        if (m_IsFound == false) m_Renderer.material = m_OverMaterial;
        if (m_audio != null) m_audio.Play();
    }


    //Handle the Out event -PUt material back to normal, turn particle system on and play any sfx
    private void HandleOut()
    {
        Debug.Log("Show out state");
        if (m_ShowHideObject != null) m_ShowHideObject.SetActive(false);
        if (m_IsFound == false) m_Renderer.material = m_NormalMaterial;
        else m_Renderer.material = m_ClickedMaterial;
        if (m_audio != null) m_audio.Pause();
    }


    //Handle the Click event - Change material to found, add 1 to the score.
    private void HandleClick()
    {
        Debug.Log("Show click state");
        m_Renderer.material = m_ClickedMaterial;
        if (m_IsFound == false)
        {
            gameStatus.AddScore(scoreValue);
            m_IsFound = true;         
        }
        
    }


    //Handle the DoubleClick event - Show the info panel about the object and hazard
    private void HandleDoubleClick()
    {
        Debug.Log("Show double click");
        m_Renderer.material = m_DoubleClickedMaterial;  
                                                        
        //GameObject canvas = GameObject.FindWithTag("TextUICanvas");
        //Show TextUICanvas - Moved to top of UI sub-action to find runtime error
        m_canvas.SetActive(true);
        Text[] textValue = m_canvas.GetComponentsInChildren<Text>();
        textValue[0].text = HazardTitle;
        textValue[1].text = "H: " + HazardDescription;
        textValue[2].text = "R: " + Risks;
        textValue[3].text = "C: " + Controls;
        

        
    }

   
}

