﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;


public class InteractiveIpad : MonoBehaviour
{ 
    [SerializeField] private VRInteractiveItem m_InteractiveItem;
    [SerializeField] private GameObject UItoHide;



     private void Awake()
    {

    }


    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_InteractiveItem.OnClick += HandleClick;
        m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_InteractiveItem.OnClick -= HandleClick;
        m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
    }


    //Handle the Over event
    private void HandleOver()
    {
        Debug.Log("Show over state");

    }


    //Handle the Out event
    private void HandleOut()
    {
        Debug.Log("Show out state");

    }


    //Handle the Click event
    private void HandleClick()
    {
        Debug.Log("Show click state");
        UItoHide.SetActive(false);

    }


    //Handle the DoubleClick event
    //Show the info panel about the object double clicked
    private void HandleDoubleClick()
    {
        Debug.Log("Show double click");
        UItoHide.SetActive(false);
    }
}