﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameStatus : MonoBehaviour {
    public int score;
    private int currentscore;
    public Text  ScoreText;
    public int TotalScore;
    
	// Use this for initialization
	void Start () {
        score = 0;
        UpdateScore();
	}
	
	// Update is called once per frame
	void Update () {
    
	}
    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore ()
    {
        ScoreText.text = "Found: " + score;
        if (score >= TotalScore) ScoreText.text = "All found.";

    }
}

