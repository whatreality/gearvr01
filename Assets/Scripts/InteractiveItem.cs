﻿using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;


// This script is used to react to gazepointer actions. 
public class InteractiveItem : MonoBehaviour
{
    // Items used to control objects, camera and scenes using this script.  Leave blank to have no effect. 

    [SerializeField] private VRInteractiveItem m_InteractiveItem;   //The VRInteractiveItem script needs to be attached to the object.
    
    //Materials and gameobjects used for actions
    [SerializeField] private Material m_NormalMaterial;
    [SerializeField] private Material m_OverMaterial;
    [SerializeField] private Material m_ClickedMaterial;
    [SerializeField] private Material m_DoubleClickedMaterial;
    [SerializeField] private GameObject m_ShowHideObject;            // Object that his displayed on hover and hidden on out
    [SerializeField] private AudioSource m_audio;                    // sound fx when hovered
    [SerializeField] private Renderer m_Renderer;                   // Object that is changed when interacted with

    [SerializeField] private string m_NewScene;                      // Scene to load on click.

    //variables for portals
    [SerializeField] private GameObject m_PortalObject;              // Item to jump to ($+2.1y) for portal interactions. NOT YET IMPLEMENTED
    [SerializeField] private GameObject m_CameraController;         // Use parent object of camera or playercontroller
    

    private void Start()
    {
        m_ShowHideObject.SetActive(false);

    }

    private void Awake()
    {
        m_Renderer.material = m_NormalMaterial;
        
    }


    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_InteractiveItem.OnClick += HandleClick;
        m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_InteractiveItem.OnClick -= HandleClick;
        m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
    }


    //Handle the Over event
    private void HandleOver()
    {
        Debug.Log("Show over state");
        m_Renderer.material = m_OverMaterial;
        if (m_ShowHideObject != null) m_ShowHideObject.SetActive(true);
        if (m_audio != null) m_audio.Play();
    }


    //Handle the Out event
    private void HandleOut()
    {
        Debug.Log("Show out state");                
        m_Renderer.material = m_NormalMaterial;                         //Added particle system in hierachy to test
        if (m_ShowHideObject != null) m_ShowHideObject.SetActive(false);
        if (m_audio != null) m_audio.Pause();

    }


    //Handle the Click event
    private void HandleClick()
    {
        Debug.Log("Show click state");
        if (m_ClickedMaterial != null) m_Renderer.material = m_ClickedMaterial;

        // Load to new scene if set
        if (m_NewScene != null)
        {
            SceneManager.LoadScene(m_NewScene, LoadSceneMode.Single);
        }

        // Move to portal if set
        if (m_PortalObject != null)
        {
            //get and setup footstep audio
            GameObject m_portalaudio = GameObject.Find("Footsteps");
            AudioSource m_footsteps = m_portalaudio.GetComponent<AudioSource>();

            // Move the camera to the location of the portal item and a Y of 1.8
            Vector3 npos = m_PortalObject.transform.position;   // get position of portal item
            npos += Vector3.up * 1.8f;                          // move the position up to eye height
            m_CameraController.transform.position = npos;       // change the camera position to the new position
            m_footsteps.Play();
        }
        
    }


    //Handle the DoubleClick event
    //Show the info panel about the object double clicked
    private void HandleDoubleClick()
    {
        Debug.Log("Show double click");
        m_Renderer.material = m_DoubleClickedMaterial;
    }

    

}

