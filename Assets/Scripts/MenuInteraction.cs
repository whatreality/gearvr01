﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuInteraction : MonoBehaviour {
    public string NewScene;
    [SerializeField]
    private VRInteractiveItem m_InteractiveItem;
    public Button button;
    public Color hovercolor;
    public Color normalcolor;
    [SerializeField]
    private VRCameraFade m_VRCameraFade;           // Reference to the script that fades the scene to black.

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void Awake()
    {
        
    }


    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_InteractiveItem.OnClick += HandleClick;
        m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_InteractiveItem.OnClick -= HandleClick;
        m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
    }


    //Handle the Over event
    private void HandleOver()
    {
        Debug.Log("Show over state");
       // ColorBlock cb = button.colors;
       //cb.normalColor = hovercolor;
       // button.colors = cb;
       
    }


    //Handle the Out event
    private void HandleOut()
    {
        Debug.Log("Show out state");
       // ColorBlock cb1 = button.colors;
       // cb1.normalColor = normalcolor;
       // button.colors = cb1;

    }


    //Handle the Click event
    private void HandleClick()
    {
        Debug.Log("Show click state");
        SceneManager.LoadScene(NewScene);

    }


    //Handle the DoubleClick event
    //Show the info panel about the object double clicked
    private void HandleDoubleClick()
    {
        Debug.Log("Show double click");
    }

    private IEnumerator FadeToMenu()
    {
        // Wait for the screen to fade out.
        yield return StartCoroutine(m_VRCameraFade.BeginFadeOut(true));

        // Load the main menu by itself.
        SceneManager.LoadScene(NewScene, LoadSceneMode.Single);
    }

    void  OpenHazardID ()
    {
        SceneManager.LoadScene("HazardID");
    }
}
