﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Slideshow : MonoBehaviour {

    float elapsedTime = Mathf.Infinity;

    // Object and text to be changed druing slideshow
    public AudioSource m_Audio;         // background music or narration
    public Renderer m_TVImage;         // item that receives the action materials
    public Text m_TVText;               // item that receives changing text 
    public Text m_Timer;                // Display elaspedTime
    public Text m_NextChange;           // Display change variable to test if statement

    private int m_CurrentItem;          // use this to keep track of which slideshow item to show or is showing


    //public int NumberChanges;
    public float[] m_ChangeTimes;  // Length of time slide stays on before changing
    public Material[] m_Materials; // List of materials to show as each slide
    public string[] m_Texts;        // List of text items to show as part of each slide
    

    // Use this for initialization
    void Start () {
        elapsedTime = 1.0f;
        m_TVImage.material = m_Materials[0];
        m_TVText.text = m_Texts[0];
        m_CurrentItem = 0;
        m_Audio.Play();
        m_Timer.text = elapsedTime.ToString();
        m_NextChange.text = m_ChangeTimes[m_CurrentItem].ToString();
    }
	
	// Update is called once per frame
	void Update () {
        
        m_NextChange.text = m_ChangeTimes[m_CurrentItem].ToString();
        if (elapsedTime > m_ChangeTimes[m_CurrentItem])
        {
            m_CurrentItem += 1;
            m_TVText.text = m_Texts[m_CurrentItem];
            m_TVImage.material = m_Materials[m_CurrentItem];
            m_NextChange.text = m_CurrentItem.ToString();
            elapsedTime = 0.0f;
        }
        elapsedTime += Time.deltaTime;
        m_Timer.text = elapsedTime.ToString();
        
    }
}
